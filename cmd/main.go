package main

import (
	"log"

	"gitlab.com/w1650/remote-shell-executor/cmd/remoter"
)

func main() {
	// init. remoter-executer service
	if err := remoter.Run(); err != nil {
		log.Fatalln()
	}
}
