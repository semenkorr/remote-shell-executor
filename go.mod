module https://gitlab.com/w1650/remote-shell-executor

go 1.16

require (
	github.com/serge64/env v0.0.1 // indirect
	go.mongodb.org/mongo-driver v1.9.0 // indirect
)